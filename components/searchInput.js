import React from 'react';
import { View, StyleSheet, Image, TextInput, TouchableHighlight } from 'react-native';
import MagnifyingGlass from '../img/search.png';
import Camera from '../img/camera.png';

const SearchInput = (props) => {
    return (
        <View style={styles.container} >
            <Image
                source={MagnifyingGlass}
                style={styles.search} />
            <TextInput
                returnKeyType='done'
                style={styles.textBox}
                onChangeText={props.textChange}
                placeholder="Financial term" />
            <TouchableHighlight onPress={props.scanImage}>
                <Image
                    source={Camera} />
            </TouchableHighlight>    
        </View>
        
    );
};

const styles = StyleSheet.create({
    container: {
        width: 311,
        height: 44,
        borderWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 10
    },
    search: {
        width: 36,
        height: 36,
        margin: 4
    },
    textBox: {
        height: 44,
        width: 267
    }
});

export default SearchInput;