import React from 'react';
import { StyleSheet, View, Text, TextInput, Button, ScrollView, TouchableHighlight, Image} from 'react-native';
import cheese from 'cheese-name';
import ButtonBg from '../img/button.png';

const styles = StyleSheet.create({
    correctAnswer: {
        alignSelf: 'center',
        fontWeight: "600",
        fontSize: 18
    },
    incorrectAnswer: {
        alignSelf: 'center',
        fontWeight: "600",
        fontSize: 18,
        color: 'red'
    },
    buttonWrapper: {
        width: 311,
        height: 44,
        alignSelf: 'center',
        margin: 10
    },
    buttonText: {
        position: 'absolute',
        alignSelf: 'center',
        top: 12,
        fontSize: 18
    },
    buttonBg: {
        position: 'absolute',
        width: 311,
        height: 44
    },
    question: {
        alignSelf: 'center',
        fontWeight: "600",
        fontSize: 18
    }
});

export default class QuizPage extends React.Component {
    constructor() {
        super();
        this.state = {
            questionCounter: 0
        }
    }

    checkAnswer = (answer, correctAnswer) => {
        const correct = answer == correctAnswer;
        this.setState({
            correctAnswer: correct
        });
    }

    render() {
        const cheeseName = cheese();
        const questions = this.props.content.questions.map((question, i) => {
            const potentialAnswers = question.potentialAnswers.map((answer, i) => {
                return (
                    <Button key={i} title={answer} onPress={() => {
                        this.checkAnswer(answer, question.correctAnswer)
                    } } />
                );
            });
            return (
                <View key={ i }>
                  <Text style={styles.question}>Question:
                    { question.question }
                  </Text>
                  <View>
                    { potentialAnswers }
                  </View>
                  { (this.state.questionCounter + 1) % this.props.content.questions.length > 0 ?
                    <TouchableHighlight onPress={() => {    
                            this.setState({
                                correctAnswer: undefined,
                                questionCounter: this.state.questionCounter + 1
                            })
                    }}>
                        <View style={styles.buttonWrapper}>
                            <Image source={ButtonBg} style={styles.buttonBg} />
                            <Text style={styles.buttonText}>Go To Next Question</Text>
                        </View>
                    </TouchableHighlight> :
                    <TouchableHighlight onPress={() => {
                        this.props.resetState();
                    }}>
                        <View style={styles.buttonWrapper}>
                            <Image source={ButtonBg} style={styles.buttonBg} />
                            <Text style={styles.buttonText}>Restart</Text>
                        </View>
                    </TouchableHighlight> }
                </View>
            )
        });
        return (
            <View>
                <ScrollView style={styles.scrollView}>
                { questions[this.state.questionCounter] }
                { this.state.correctAnswer == true ? <Text style={ styles.correctAnswer }>Hooray, you got the right answer. Here's a random cheese name for you - { cheeseName }
                                                     </Text>
                  : null }
                { this.state.correctAnswer == false ? <Text style={ styles.incorrectAnswer }>Incorrect, try again</Text> : null }
              </ScrollView>
            </View>
        )
    }
}