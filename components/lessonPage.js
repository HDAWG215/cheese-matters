import React from 'react';
import { StyleSheet, View, Text, TextInput, TouchableHighlight, Image } from 'react-native';
import ButtonBg from '../img/button.png';


const LessonPage = (props) => {
    return (
        <View>
            <Text style={styles.title}>
                { props.content.title }
            </Text>
            <Text style={styles.description}>
                { props.content.description }
            </Text>
            <TouchableHighlight onPress={props.goToQuiz}>
                <View style={styles.buttonWrapper}>
                    <Image source={ButtonBg} style={styles.buttonBg} />
                    <Text style={styles.buttonText}>Go To Quiz</Text>
                </View>
            </TouchableHighlight>    
        </View>
    )
}

const styles = StyleSheet.create({
    scrollView: {
        flex: 1
    },
    title: {
        fontSize: 36,
        marginTop: 20,
        alignSelf: 'center',
        justifyContent: 'flex-start'
    },
    description: {
        fontSize: 24,
        marginTop: 20,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    buttonWrapper: {
        width: 311,
        height: 44,
        alignSelf: 'center',
        margin: 10
    },
    buttonText: {
        position: 'absolute',
        alignSelf: 'center',
        top: 12,
        fontSize: 18
    },
    buttonBg: {
        position: 'absolute',
        width: 311,
        height: 44
    }
})

export default LessonPage;