import React from 'react';
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native';
import Logo from '../img/logo.png';
import SearchInput from './searchInput';
import ButtonBg from '../img/button.png';

const InputPage = (props) => {
    return (
        <View>
            <Image source={Logo} style={styles.logo} />
            <SearchInput textChange={props.textChange} scanImage={props.scanImage} />
            <TouchableHighlight onPress={props.onPress}>
                <View style={styles.buttonWrapper}>
                    <Image source={ButtonBg} style={styles.buttonBg}  />
                    <Text style={styles.buttonText}>Submit</Text>
                </View>
            </TouchableHighlight>    
            { props.validKey == false ? <Text>
                { props.term } key not found</Text> : null }
        </View>
    )
}

const styles = StyleSheet.create({
    logo: {
        width: 350,
        height: 350,
        margin: 'auto',
        alignSelf: 'center'
    },
    buttonWrapper: {
        width: 311,
        height: 44,
        alignSelf: 'center',
        margin: 10
    },
    buttonText: {
        position: 'absolute',
        alignSelf: 'center',
        top: 12,
        fontSize: 18
    },
    buttonBg: {
        position: 'absolute',
        width: 311,
        height: 44 
    }
});

export default InputPage;