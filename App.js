import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import cheese from 'cheese-name';
import InputPage from './components/inputPage';
import LessonPage from './components/lessonPage';
import content from './content/content.json';
import QuizPage from './components/quizPage';

const initialState = {};

export default class App extends React.Component {
    constructor() {
        super();
        this.state = Object.assign({}, initialState);

        this.inputTextChange = this.inputTextChange.bind(this);
        this.onPress = this.onPress.bind(this);
        this.goToQuiz = this.goToQuiz.bind(this);
        this.checkAnswer = this.checkAnswer.bind(this);
        this.resetState = this.resetState.bind(this);
    };

    inputTextChange = (text) => {
        const inputText = text.toLowerCase();
        this.setState({
            text: inputText
        });
    };

    onPress = () => {
        const isValidKey = content[this.state.text];
        isValidKey ?
            this.setState({
                termSubmitted: true,
                validKey: true
            }) :
            this.setState({
                validKey: false
            });
    };

    goToQuiz = () => {
        this.setState({
            quizPage: true
        });
    }

    checkAnswer = (answer, correctAnswer) => {
        const correct = answer == correctAnswer;
        this.setState({
            answerCorrect: correct
        });
    }
    resetState = () => {
        this.setState({
            quizPage: false,
            answerCorrect: undefined,
            termSubmitted: false,
            text: undefined,
            validKey: undefined
        })
    }

    render() {
        return (
            <View style={ styles.container }>
              { !this.state.quizPage ?
                (this.state.termSubmitted ?
                    LessonPage({
                        content: content[this.state.text],
                        goToQuiz: this.goToQuiz
                    }) :
                    InputPage({
                        textChange: this.inputTextChange,
                        onPress: this.onPress,
                        validKey: this.state.validKey,
                        term: this.state.text
                    })
                ) :
                <QuizPage content={ content[this.state.text] } checkAnswer={ this.checkAnswer } correctAnswer={ this.state.correctAnswer } resetState={ this.resetState } /> }
            </View>
            );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: 'center',
        flex: 1,
        justifyContent: 'space-around'
    }
});